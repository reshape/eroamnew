<?php

namespace App\Libraries;
use Cache;

class ApiCache {

	public function save($key, $value) {
		if (Cache::has($key)) {
			Cache::forget($key);
		}
		Cache::forever($key, $value);
	}

	public function get_all_cities() {
		$cities = http('get', 'map/city');
		$this->save('cities', $cities);
		return getAllCities();
	}

	public function get_all_countries() {
		$countries = http('get', 'map/get/all/countries');
		$this->save('countries', $countries);		
		return getAllCountries();
	}

	public function get_all_hotel_categories() {
		$hotel_categories = http('get', 'hotel-categories');
		// session()->put('hotel_categories', $hotel_categories);
		$this->save('hotel_categories', $hotel_categories);

		return getAllHotelCategories();
	}

	public function get_all_labels(){
		$labels 	= http( 'get', 'labels');
		// session()->put('labels', $labels);
		$this->save('labels', $labels);

		return getAllLabels();
	}
	public function get_traveller_options(){
		$headers = [
				'X-Authorization' => '7b94b3635849f1da030358d5724c72c1f320ca5c',
				'Origin' => 'http://app.testeroam.com'
			];
		$travellers = http( 'get', 'traveler-options',[],$headers);
		$this->save('travellers', $travellers);
		
		return Cache::get($travellers);
	}

    public function get_all_countries_bookingspro() {
        $countries = http('get', 'map/get/all/countriesBookingPro');

        $this->save('countriesBookingsPro', $countries);

        return getAllCountriesBookingspro();
    }
}