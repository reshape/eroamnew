$(document).ready(function(){
    $("#nationality option[value='disabled']").attr("disabled", true);
	
	var csrf_token = $("#csrf_token").val();
    $.validator.addMethod("fieldPresent",
        function(value, element,options) {
			var result = true;
			
			if($('input[name="'+options.data1+'"]').val() != '' || $('input[name="'+options.data2+'"]').val() != ''){
				if(value == ''){
					result = false;
				}
			}
			
            // return true if username is exist in database
            return result;
        }
        
    );
	
	$.validator.addMethod("profilePWcheck", function(value) {
		if(value == ''){
			return true
		}
		
		return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w]))(?=.*\d).{8,}.+$/.test(value) 
	}, "Password should be min. eight characters, including one uppercase one lowercase one number and one special character");
	
	$.validator.addMethod("profileNumber", function(value) {
		//var phoneno = /^\+?([0-9]{2})\)?[-]?([0-9]{3})[ ]?([0-9]{3})?[-]?([0-9]{4})$/;
		//var phoneno = /^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/;   6-08-2018
        var phoneno = /^[0-9]{7,15}$/;
		if (value.match(phoneno)) {
			return true;
		}
		else {
			return false;
		}
		
		//return /^(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}.+$/.test(value) 
	}, "Contact number should be numeric or between 7 to 15.");
	
	$.validator.addMethod("profileVerifyEmail",
        function(value, element) {
            var reg_user_email = $("#registered_user_id").val();
            var result = false;
            if(value == reg_user_email)
            {
                return true;
            }
            $.ajax({
                type:"POST",
                async: false,
                url: "/email/validate", // script to validate in server side
                data: {"reg_email": value,"_token": csrf_token},
                success: function(data) {
                    heightSidemenu();
                    var final_result = '';
                    if(data.success == 1)
                    {
                        if(data.social == 1)
                        {
                            final_result = false;
                        }
                        else
                        {
                            final_result = true;    
                        }                        
                    }
                    else
                    {
                        final_result = false;
                    }
                    result = final_result;
                }
            });
            // return true if username is exist in database
            return result;
        }
        
    );
	
	$("#profile_step1_form").validate({
        rules: {
            title: {
                required: true,
            },
            first_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            email: {
                required: true,
                email:true,
                profileVerifyEmail : true
            },
            contact_no: {
                required: true,
                profileNumber: true,
            },       
            old_password: {
                fieldPresent : { data1:"new_password",data2:"confirm_password"}
            },
			new_password: {
                minlength: 8,    
                fieldPresent : { data1:"old_password",data2:"confirm_password"},
				profilePWcheck : true
            },
            confirm_password: {
			   fieldPresent : { data1:"new_password",data2:"old_password"},
               equalTo : "#new_password",
            },
            pref_gender: {
                required: true
            },
            pref_age_group_id: {
                required: true
            },
            pref_nationality_id: {
                required: true
            },
            currency: {
                required: true
            }
        },
        messages: {
            first_name: {
                required: "Please enter First Name."
            },
            last_name: {
                required: "Please enter Last Name."
            },
            email: {
                required: "Please enter Email Address.",
                profileVerifyEmail: "Email already exists"
            },
            confirm_password: {
                equalTo: 'New Password and Confirm Password must be same.'
            },
            pref_nationality_id: {
                required: "Please select Nationality.",
            },
            pref_gender: {
                required: "Please select Gender.",
            },
            currency: {
                required: "Please select Currency.",
            },
            old_password:{
                fieldPresent : "Please enter old password",
                minlength : "Minimum Length of new password must be 8 characters long",
            },
			new_password:{
                fieldPresent : "Please enter new password",
                minlength : "Minimum Length of new password must be 8 characters long",
            },
			confirm_password:{
                fieldPresent : "Please enter confirm password",
                minlength : "Minimum Length of new password must be 8 characters long",
            },
        },
        highlight: function (element) { // hightlight error inputs
            //heightSidemenu();
        },
        errorPlacement: function (label, element) {
            label.insertAfter($(element).parents('div.fildes_outer'));
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    $("#profile_step2_form").validate({
        rules: {
            phy_address_1: {
                required: true,
            },
            phy_country: {
                required: true
            },
            phy_state: {
                required: true
            },
            phy_city: {
                required: true
            },
            phy_zip: {
                required: true,
                maxlength: 12,
            },
            bill_address_1: {
                required: true
            },
            bill_country: {
                required: true
            },
            bill_state: {
                required: true
            },
            bill_city: {
                required: true
            },
            bill_zip: {
                required: true,
                maxlength: 12,
            }
        },
        messages: {
            phy_address_1: {
                required: "Please enter Address"
            },
            phy_country: {
                required: "Please enter Country"
            },
            phy_state: {
                required: "Please enter State"
            },
            phy_city: {
                required: "Please enter City"
            },
            phy_zip: {
                required: "Please enter ZIP"
            },
            bill_address_1: {
                required: "Please enter Address"
            },
            bill_country: {
                required: "Please enter Country"
            },
            bill_state: {
                required: "Please enter State"
            },
            bill_city: {
                required: "Please enter City"
            },
            bill_zip: {
                required: "Please enter ZIP"
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter($(element).parents('div.fildes_outer'));
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
$("#profile_step4_form").validate({
        rules: {
            phy_address_1: {
                required: true,
            },
            phy_country: {
                required: true
            },
            phy_state: {
                required: true
            },
            phy_city: {
                required: true
            },
            phy_zip: {
                required: true,
                maxlength: 12,
            },
            email: {
                required: true
            },
            contact_no: {
                required: true
            }
        },
        messages: {
            phy_address_1: {
                required: "Please enter Address"
            },
            phy_country: {
                required: "Please enter Country"
            },
            phy_state: {
                required: "Please enter State"
            },
            phy_city: {
                required: "Please enter City"
            },
            phy_zip: {
                required: "Please enter ZIP"
            },
            email: {
                required: "Please enter Email"
            },
            contact_no: {
                required: "Please enter Contact Number"
            }
        },
        errorPlacement: function (label, element) {
            label.insertAfter($(element).parents('div.fildes_outer'));
        },
        submitHandler: function (form) {
            form.submit();
        }
    });


});
	