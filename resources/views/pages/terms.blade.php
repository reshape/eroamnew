@extends('layouts.static') @section('custom-css')
<style>
    .wrapper {
       background-image: url( {{ url( 'images/bg1.jpg' ) }} ) !important;
    }

    .top-margin {
        margin-top: 5px;
    }

    .container-padding {
        padding-left: 35px;
        padding-right: 35px;
    }

    .top-bottom-paddings {
        padding-top: 20px;
        padding-bottom: 20px;
    }

    .white-box {
        background: rgba(255, 255, 255, 0.9);
        border: solid thin #9c9b9b !important;
    }

    .wrapper {
        background-attachment: fixed;
    }

    #terms p {
        line-height: 1.3em;
        text-align: justify;
    }

    .padding-20 {
        padding-left: 20px;
        padding-right: 20px;
    }

</style>
@stop @section('content')

<section class="content-wrapper">
    <h1 class="hide"></h1>
    <article class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="border-bottom">Terms &amp; Conditions</h2>
                    <div class="mt-4">
                        {!! $data['terms_of_service'] !!}   
                    </div>
                </div>
            </div>
        </div>
    </article>
</section>


@stop @section( 'custom-js' ) @stop
