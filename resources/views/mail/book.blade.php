<?php //echo "<pre>";print_r(session()->get('orderData'));
$data = session()->get('orderData');
//exit; ?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>eRoam</title>
</head>

<body style="margin: 0px; background: #e3e5e7;">
	<?php 
		$logo = getDomainLogo();
		if(!$logo) {
			$logo = "http://dev.eroam.com/images/email-logo-footer.png";
		}
	?>
    <table width="100%" style="background: #e3e5e7">
        <tr>
            <td>

                <table style="margin:0 auto; background: #ffffff;" width="760px" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th style="background-color: #212121; padding-top:20px; padding-bottom: 20px; text-align: center;">
                                <a href="#"><img src="<?php echo $logo; ?>" width="180px" alt="" /> </a>
                            </th>
                        </tr>
					</thead>
					<tbody>
						<tr>
							<td style="background: #fafafa; border-bottom: 1px solid #f0f0f0; padding-top: 5px; padding-bottom: 5px;">
								<table width="100%">
									<?php /*<tr>
                                        <td style="text-align: center">
                                            <img src="http://dev.eroam.com/images/user_icon.png" alt="" />
                                        </td>
                                    </tr>*/ ?>
                                    <tr>
										<td style="text-align: center">
 											<p style="font-family: Arial; font-size: 18px; color: #212121; margin-top: 0px; margin-bottom: 0px;  "><strong> Hi, <?php echo ucwords($data['passenger_info'][0]['passenger_first_name'].' '. $data['passenger_info'][0]['passenger_last_name']);?>!</strong></p>
                                        </td>
									</tr>
								</table>
							</td>
                        </tr>
						<tr>
							<td>
								<h1 style="font-size: 20px; color: #212121; letter-spacing: 0.71px; line-height: 18px; margin-top: 20px; margin-bottom: 20px;margin-left:3px;">Booking Information</h1>
							</td>
						</tr>
                        <tr>
							<td>
								<table style="width: 100%;">
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;font-size:14px;">
							<tr>
								<td colspan="2">
									<h4 style="font-size: 20px; line-height: 22px; font-weight: 500; margin-top: 0px; margin-bottom: 20px;"><strong>Personal Details</strong></h4>
								</td>
							</tr>
							<tr>
								<td colspan="2"><strong>Booking Id:</strong> <?php echo $data['invoiceNumber'];?></td>
							</tr>
							<tr>
								<td width="50%"><strong>Customer Name:</strong> <?php echo ucwords($data['passenger_info'][0]['passenger_first_name'].' '. $data['passenger_info'][0]['passenger_last_name']);?></td>
								<td width="50%"><strong>Customer Email:</strong> <?php echo $data['passenger_info'][0]['passenger_email'];?></td>
							</tr>
							<tr>
								<td width="50%"><strong>Customer Contact:</strong> <?php echo $data['passenger_info'][0]['passenger_contact_no'];?></td>
								
							</tr>
							<tr>
								<td width="50%"><strong>Customer Dob:</strong> <?php echo $data['passenger_info'][0]['passenger_dob'];?></td>
								<?php /*<td width="50%"><strong>Customer Country:</strong> <?php //echo $data['passenger_info'][0]['passenger_country_name'];?></td>*/ ?>
							</tr>
							<tr>
								<td width="50%"><strong>Days:</strong> <?php echo $data['total_days'];?></td>
								<td width="50%"><strong>Total Amount:</strong> AUD $<?php echo number_format($data['finalTotalCost'],2);/*echo $data['cost_per_day'];?> per day (TOTAL AUD <?php echo $data['total_per_person'];  (per person))*/?></td>
							</tr>
							<tr>
								<td width="50%"><strong>Date:</strong> <?php echo date("j M Y", strtotime($data['from_date']));?> - <?php echo date("j M Y", strtotime($data['to_date']));?></td>
								<td width="50%"><strong>Total Paid Amount:</strong> AUD $<?php echo number_format($data['totalPaidAmount'],2);?></td>
							</tr>
							<tr>
								<?php 
								if($data['child'] >= 1){
									$child = ", ".$data['child']." Child";
								}else{
									$child = "";
								}
								$traveller_text =  $data['adult']." Adult".$child;?>
								<td width="50%"><strong>Travellers:</strong> <?php echo $traveller_text;?></td>
							</tr>
						</table>
					</td>
				</tr>
                <?php

                $last_key = count($data['leg']) -1;
                foreach ($data['leg'] as $key => $value) {

                    ?>
				<tr>
					<td>
						<table border="0" width="100%" cellspacing="2" cellpadding="2" style="padding: 15px; position:relative; margin-bottom:20px; background:#FFFFFF; border:2px solid #efefef;font-size:14px;">
							<tr>
								<td colspan="2">
									<h4 style="font-size: 20px; line-height: 22px; margin-top: 0px; margin-bottom: 20px;">{{$value['from_city_name']}}, {{$value['country_code']}}</h4>
								</td>
							</tr>
							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/hotel-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;"/> &nbsp; Accommodation</h5></td>
							</tr>
                            <?php
                            if(isset($value['hotel']) && !empty($value['hotel'])){
                            ?>
							<tr>
								<td width="50%"><strong>Accommodation Name:</strong> {{$value['hotel'][0]['leg_name']}}</td>
								<td width="50%"><strong>Duration: </strong>{{$value['hotel'][0]['nights']}} Nights</td>
							</tr>
							<tr>
								<td width="50%"><strong>Address:</strong> {{!empty($value['hotel'][0]['address']) ? $value['hotel'][0]['address'] : ''}}</td>
								<?php if(!empty($value['hotel'][0]['provider_booking_status'])){ ?>
								<td width="50%"><strong>Booking Id: </strong>{{$value['hotel'][0]['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%"><strong>Booking Status:</strong> Not Confirmed *</td>
								<?php } ?>
							</tr>
							<tr>
								<td width="50%"><strong>Check-in: </strong>{{ date( 'j M Y', strtotime($value['hotel'][0]['checkin']))}}</td>
								<td width="50%"><strong>Check-out: </strong>{{ date( 'j M Y', strtotime($value['hotel'][0]['checkout']))}}</td>
							</tr>
							<tr>
								<td width="50%"><strong>Price: </strong>{{$value['hotel'][0]['currency']}} {{$value['hotel'][0]['price']}}</td>

								<?php if(isset($value['hotel'][0]['HotelFees'])) { ?> 
									<td width="50%"><strong>Hotel Fees:</strong> 
										{{$value['hotel'][0]['currency'].' '.$value['hotel'][0]['HotelFees']}}</td>
								<?php } else { echo '<td width="50%"></td>';} ?>
							</tr>

							<?php 
								if(isset($value['hotel'][0]['BedTypes']) && $value['hotel'][0]['BedTypes']!='') { 
									$bedTypeId = $value['hotel'][0]['bedTypeId'];

									if($value['hotel'][0]['BedTypes']['@size'] == 1){
										$bedTypeDescription = ucwords($value['hotel'][0]['BedTypes']['BedType']['description']);
									} else {
										$i = 0;
										foreach ($value['hotel'][0]['BedTypes']['BedType'] as $BedType) {
					                  		$i++;
					                  		if(isset($bedTypeId) && $bedTypeId == $BedType['@id']){
					                  			$bedTypeDescription = ucwords($BedType['description']);
					                  		} elseif($i == 0) {
					                  			$bedTypeDescription = ucwords($BedType['description']);
					                  		} else {
					                  			$bedTypeDescription = '';
					                  		}
					                  	}
					                }
							?> 
								<tr>
									<td colspan="2"><strong>Bed Preferences:</strong> 
									{{$value['hotel'][0]['hotel_room_type_name']}}, <strong>{{$bedTypeDescription}}</strong></td>
								</tr>
							<?php } ?>
							
							<?php if(isset($value['hotel'][0]['cancellationPolicy'])) { ?> 
							<tr>
								<td colspan="2"><strong>Cancellation Policy: </strong>{{$value['hotel'][0]['cancellationPolicy']}}</td>
							</tr
							<?php } ?>

							<?php }else{ ?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan="2"></td>
							</tr>

							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/activity-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;font-size:14px;"/> &nbsp; Activity Summary</h5></td>
							</tr>
							<?php
                            if(isset($value['activities']) && !empty($value['activities'])){
                            foreach ($value['activities'] as $key1 => $value1) {

								?>

							<tr>
								<td width="50%"><strong>Activity Name:</strong> {{$value1['leg_name']}}</td>
								<td width="50%"><strong>Date:</strong> {{ date( 'j M Y', strtotime($value1['from_date']))}}</td>
							</tr>
							<tr>
								<td width="50%"><strong>Price:</strong> {{$value1['currency']}} {{$value1['price']}}</td>
                                <?php if($value1['provider_booking_status'] == 'CONFIRMED'){ ?>
								<td width="50%"><strong>Booking Id: </strong>{{$value1['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%"><strong>Booking Status: </strong>Not Confirmed *</td>
								<?php } ?>
							</tr>
							@if($value1['provider'] == 'viator')
							<tr>
								<td width="50%"><strong>Provider Name:</strong> Viator</td>
								<td width="50%"><strong>Voucher Link:</strong> <a href="{{$value1['voucher_url']}}" target="_blank">Click Here</a></td>
							</tr>
							@endif
                            <?php } }else{ ?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
                            <?php } ?>
							<tr>
								<td colspan="2"></td>
							</tr>
							<?php if($key != $last_key) {?>
							<tr>
								<td colspan="2"><h5 style="font-size: 16px; margin: 0px;"><img src="{{ url( 'images/transport-icon.png' ) }}" alt="" style="color: #000; margin-right: 5px; position: relative; top: 2px;font-size:14px;"/> &nbsp;Transport</h5></td>
							</tr>
							<?php } if(isset($value['transport']) && !empty($value['transport']) && $key != $last_key){ ?>
                            <?php
                            $depart = $value['transport'][0]['departure_text'];
                            $arrive = $value['transport'][0]['arrival_text'];
                            if($value['transport'][0]['leg_name'] == 'Flight'){
                                if(!empty($value['transport'][0]['booking_summary_text'])){
                                    $leg_depart = explode("Depart:",$value['transport'][0]['booking_summary_text'],2);
                                    $leg_arrive = explode("Arrive:",$value['transport'][0]['booking_summary_text'],2);

                                    if(isset($leg_depart[1])){
                                        $depart = explode("</small>",$leg_depart[1],2);
                                        $depart = $depart[0];
                                    }else{
                                        $depart = '';
                                    }
                                    if(isset($leg_arrive[1])){
                                        $arrive = explode("</small>",$leg_arrive[1],2);
                                        $arrive = $arrive[0];
                                    }else{
                                        $arrive = '';
                                    }
                                    $depart = $value['transport'][0]['departure_text'];
                                	$arrive = $value['transport'][0]['arrival_text'];
                                }else{
                                    $depart = '';
                                    $arrive = '';
                                }
                            }
                            if($value['transport'][0]['provider'] == 'busbud'){


                                $depart = $value['transport'][0]['departure_text'];
                                $arrive = $value['transport'][0]['arrival_text'];

                            }

                            ?>
                            <?php
                            $leg_name = explode('Depart:', $value['transport'][0]['booking_summary_text']);
                            if(isset($value['transport'][0]['booking_summary_text']) && !empty($value['transport'][0]['booking_summary_text'])){
                                $leg_name = trim(strip_tags($leg_name[0]));
                            }else{
                                $leg_name = strip_tags($value['transport'][0]['leg_name']);
                            }
                            ?>
							<tr>
								<td colspan="2"><strong>Service:</strong>{{$leg_name}}</td>
							</tr>
							<tr>
								<td width="50%"><strong>Depart:</strong> {{strip_tags($depart)}}</td>
								<td width="50%"><strong>Arrive:</strong> {{strip_tags($arrive)}}</td>
							</tr>
							<tr>
								<td width="50%"><strong>Price: </strong>{{$value['transport'][0]['currency']}} {{$value['transport'][0]['price']}}</td>
								<?php if($value['transport'][0]['provider_booking_status'] == 'CONFIRMED'){ ?>
								<td width="50%"><strong>Booking Id: </strong>{{$value['transport'][0]['booking_id']}}</td>
								<?php }else{ ?>
								<td width="50%"><strong>Booking Status:</strong> Not Confirmed *</td>
								<?php } ?>
							</tr>

							<?php }else{ 
								if($key != $last_key) {?>
							<tr>
								<td colspan="2">Own Arrangement</td>
							</tr>
							<?php }} ?>
						</table>
					</td>
				</tr>
				<?php } ?>
			</table>
					</td>
                        </tr>
					</tbody>
					<tfoot>
                        <tr>
                            <td style="background-color: #394951; padding-top:6px; padding-bottom: 6px; padding-right: 6px; padding-left: 6px; text-align: center;">
								<table style="width: 100%;">

                                    <tr>
                                        <td style="font-family: Arial; font-size: 14px;"> <a href="#"><img src="<?php echo $logo; ?>" alt="" /> </a></td>
                                        <td style="color: #fff;font-size: 12px; text-align: right; font-family: Arial; ">Powered by eRoam &copy; Copyright 2018 - 2019. All Rights Reserved. Patent pending AU2016902466</td>

                                    </tr>
								</table>
							</td>
                        </tr>
					</tfoot>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
