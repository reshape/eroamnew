@extends('layouts.common')
@section('content') 
<div class="body_sec">
 <div class="loginpage" style="background-image:url(images/login-bg.jpg);">
   <div class="container">
      <div class="loginouter">
         <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
               <a class="nav-link pt-3 pb-3 active" id="signin-tab" data-toggle="tab" href="#signin" role="tab" aria-controls="home" aria-selected="true">Forgot Password</a>
            </li>
         </ul>
         <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="signin" role="tabpanel" aria-labelledby="home-tab">
               <div class="login_icon">
                  <i class="fa fa-user-o" aria-hidden="true"></i>
               </div>
               <div class="login_details">
                  <h4>Forgot Password</h4>
                  <p class="mb-5">Please enter your email and we'll help you to reset your password.</p>
                 @if(session()->has('success'))
                  <p class="success-box">{{ session()->get('success') }}</p>
                 @endif
                  <p class="login-error-msg text-danger" id="login-error-msg" >

                  </p>
                  <form id="forgot-form1" method="post">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                     <div class="form-group input_effects  mt-4 mb-2">
                        <input type="email" class="form-control input_effect" id="username" name="username">
                        <label for="username">Email address</label>
                        <span class="focus-border"></span>
                     </div>
                      @if(session()->has('error'))
                       <p id="error-msg">{{ session()->get('error') }}</p>
                      @endif
                     <div class="row mt-5">
                        <div class="col-sm-12 col-md-6">
                           <button type="button" class="btn  btns_input_white transform d-block w-100" id="1" onclick="window.location='/'">CANCEL</button>
                        </div>
                        <div class="col-sm-12 col-md-6">
                           <button type="submit" class="btn  btns_input_white transform d-block w-100" id="login-btn">SUBMIT</button>
                        </div>
                     </div>
                  </form>
               </div>
               <div class="clearfix"></div>
            </div>
         </div>
      </div>
   </div>
 </div>
</div>
   
@endsection

@push('scripts')
	<script type="text/javascript" src="{{ url('js/users/signin.js') }}"></script>
@endpush