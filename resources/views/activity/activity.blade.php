@extends('layouts.common')
<link rel="stylesheet" type="text/css" href="{{url('css/jcarousel.connected-carousels.css')}}">
@section('content')
    <div class="body_sec">
        <div class="itinerary_block">
            <input type="hidden" id="map-data" value="{{ json_encode( Session::get( 'map_data' ) ) }}">
            <input type="hidden" id="all-cities" value="{{ json_encode(getAllCities()) }}">
            <div class="booking-summary">
                <span class="data-loader"><i class="fa fa-circle-o-notch fa-spin"></i> @lang('home.update_data_message')</span>
            </div>

            @include('activity.partials.activity')
        </div>
    </div>

@push('scripts') 
    <style type="text/css">.bootbox-close-button { display:none}</style>
    <script src="{{url('js/api-ae.js')}}"></script>
    <script src="{{url('js/bootbox.js')}}"></script>
    <script type="text/javascript">
        var globalCurrency = "{{ ( session()->has('currency') ) ? session()->get('currency') : 'AUD' }}";
        var globalCurrency_id = "{{ ( session()->has('currency_id') ) ? session()->get('currency_id') : 1 }}";
        var listOfCurrencies = JSON.parse($('#currency-layer').val());
        @if( session()->has('search_input') )
        <?php $input = session()->get('search_input');?>
        var travel_pref = [{{ isset( $input['interests'] ) ? join(', ',  $input['interests']) : '' }}];
        @else
        var travel_pref = [];
        @endif
        $(document).on('ready', function() {     
            $('.regular').slick({
                dots: false,
                infinite: false,
                slidesToShow: 8,
                slidesToScroll: 1,
                responsive: [
                    {
                    breakpoint: 1024,
                        settings: {
                            slidesToShow: 6,
                            slidesToScroll: 1,
                        }
                    },
                    {
                    breakpoint: 600,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 2
                        }
                    },
                    {
                    breakpoint: 480,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
        });
    </script>
    <script src="{{url('js/itinerary/common.js')}}"></script>
    <script src="{{url('js/slick.js')}}"></script>
    <script src="{{url('js/custom.js')}}"></script>
    <script src="{{url('js/booking-summary.js')}}"></script>
    <script src="{{url('js/moment.js')}}"></script>
    <script src="{{url('js/map/jquery.slimscroll.js')}}"></script>
    <script src="{{url('js/owl.carousel.js')}}"></script>
	<script src="{{url('js/jquery.jcarousel.min.js')}}"></script>
	
	<link href="{{url('css/jquery-datepicker/jquery.datetimepicker.css')}}"  rel="stylesheet" />
	<script src="{{url('js/jquery.datetimepicker.full.js')}}"></script>
    
	@include('activity.activities_functions_js');
       
@endpush

@endsection