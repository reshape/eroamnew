@if(isset($leg['transport']) && !empty($leg['transport']))
  @php 
    global $finalTotalCost;
    $travelDate = explode('<br/>', $leg['transport']['departure_text']);
    $travelDate = explode('@', $travelDate[1]);
    $transport_price = '';
    if (isset($leg['transport']['price']['0']['price']) && !empty($leg['transport']['price']['0']['price'])) {
      $transport_price = number_format($leg['transport']['price']['0']['price'], 2);
    }
    $finalTotalCost = $finalTotalCost + $leg['transport']['price']['0']['price'];
    //print_r($leg['transport']);
  @endphp
  
  @if(isset($leg['transport']['provider']) && $leg['transport']['provider'] == 'mystifly')
   <!-- Mystifly Details Start -->
    @include('itinenary.partials.payment-summary-mystifly')
   <!-- Mystifly Details End -->
  @elseif ( $leg['transport'] && $leg['transport']['price'] && count($leg['transport']['transporttype']) )
  <tr>
      <td>
          <div class="tour_icon cityboxIcon"><i class="ic-directions_bus"></i> </div>
          <div class="tour_info cityboxDetails">
              <strong>{{$leg['transport']['transport_name_text']}}</strong>
              <p> {{  date('d M Y', strtotime($travelDate[0])) }}<br/>Duration: {{ ucwords(str_replace('+', '', $leg['transport']['duration'])) }}</p>
          </div>
      </td>
      <td class="text-center">${{$currency}} {{ $transport_price }}</td>
      <td class="text-center">
          <div class="price">
              {{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}
      </td>
      <td>
          <div class="text-right">
              <strong> ${{$currency}} {{$transport_price}}</strong><br>
              <a href="javascript:void(0)" class="disable_item_custom">Change Dates</a><br>
              <a href="javascript:void(0)" class="disable_item_custom">Remove From Itinerary</a>
          </div>
      </td>
  </tr>
  @endif
  @php $i++;@endphp
@else
  @if($total_leg != $key)
    <tr>
      <td colspan="4">
        <div class="tour_icon cityboxIcon"><i class="ic-directions_bus"></i> </div>
        <div class="tour_info cityboxDetails">
          <strong>Own Arrangement</strong></br></br>
        </div>
      </td>
    </tr>
  @endif
@endif
 