@php 
  
  $transport = json_decode(json_encode($leg['transport']) , true );

@endphp

@if(isset($transport['PricedItineraries']))

  @php
    $leg['transport']['PricedItineraries'] = $transport['PricedItineraries'];

    if(isset($leg['transport']['PricedItineraries']['OriginDestinationOptions'])){
      $FlightSegments = $leg['transport']['PricedItineraries']['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];
    }
    if(isset($leg['transport']['PricedItineraries']['PricedItinerary']['OriginDestinationOptions'])){
      $FlightSegments = $leg['transport']['PricedItineraries']['PricedItinerary']['OriginDestinationOptions']['OriginDestinationOption']['FlightSegments'];
    }
    if(isset($transport['ExtraServices']) && !empty($transport['ExtraServices'])){
      $ExtraServices = $transport['ExtraServices'];
    }else{
      $ExtraServices = [];
    }
    $CurrecySybmbol = '$';
    if(isset($transport['serviceId']) && !empty($transport['serviceId'])){
      $serviceId = $transport['serviceId'];
    }else{
      $serviceId = [];
    }
  @endphp
     <tr>
      @if(isset($FlightSegments))
     	@if(isset($FlightSegments['FlightSegment'][0]) && count($FlightSegments['FlightSegment']) > 1)
       <td>
     	  @foreach($FlightSegments['FlightSegment'] as $keyFlight => $Flight)
     	  @php
       
        $arrival_time         = date('H:i', strtotime($Flight['ArrivalDateTime']));
      
        $departure_time       =  date('H:i', strtotime($Flight['DepartureDateTime']));
        $arrival_date = date('l dS M Y', strtotime($Flight['ArrivalDateTime']));
        $departure_date = date('l dS M Y', strtotime($Flight['DepartureDateTime']));
        $duration = date('H', mktime(0, $Flight['JourneyDuration'])).' Hour(s) and '.date('i', mktime(0, $Flight['JourneyDuration'])) .' Minute(s)';

        $departCity = '';
        if(array_key_exists($Flight['DepartureAirportLocationCode'],$getCityAirportCode)){
          $departCity = $getCityAirportCode[$Flight['DepartureAirportLocationCode']].' - ';
        }

        $arrivalCity = '';
        if(array_key_exists($Flight['ArrivalAirportLocationCode'],$getCityAirportCode)){
          $arrivalCity = $getCityAirportCode[$Flight['ArrivalAirportLocationCode']].' - ';
        }
  	   @endphp		
       
            <div class="tour_icon cityboxIcon">
              @if($keyFlight == 0 )
              <i class="ic-directions_bus"></i>
              @else
              &nbsp;
              @endif 
            </div> 
            <div class="tour_info cityboxDetails mb-2">
             <strong>{{isset($Flight['MarketingAirlineName']) ? $Flight['MarketingAirlineName'] :''}} Flight #{{$Flight['MarketingAirlineCode']}}{{$Flight['FlightNumber']}}</strong>
                <p class="mb-0"><strong>Departs</strong>: {{$Flight['DepartureAirportLocationCode']}} {{$departCity}}{{$departure_time}} {{$departure_date}}</p>
                <p class="mb-0"><strong>Arrives</strong>:{{$Flight['ArrivalAirportLocationCode']}} {{$departCity}}{{$arrival_time}} {{$arrival_date}}</p>
                <p class="mb-0"><strong>Duration</strong>:{{ ucwords(str_replace('+', '', $duration)) }}</p>
            </div>
        @endforeach
        </td>
        <td class="text-center">${{$currency}} {{ $transport_price }}</td>
        <td class="text-center">
            <div class="price">
                {{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}
                
        </td>
      @else(isset($FlightSegments['FlightSegment']))
      @php
  		$arrival_time         = date('H:i', strtotime($FlightSegments['FlightSegment']['ArrivalDateTime']));
  		
  		$departure_time       =  date('H:i', strtotime($FlightSegments['FlightSegment']['DepartureDateTime']));
  		$arrival_date = date('l dS M Y', strtotime($FlightSegments['FlightSegment']['ArrivalDateTime']));
  		$departure_date = date('l dS M Y', strtotime($FlightSegments['FlightSegment']['DepartureDateTime']));
      $departCity = '';
      if(array_key_exists($FlightSegments['FlightSegment']['DepartureAirportLocationCode'],$getCityAirportCode)){
        $departCity = $getCityAirportCode[$FlightSegments['FlightSegment']['DepartureAirportLocationCode']].' - ';
      }

      $arrivalCity = '';
      if(array_key_exists($FlightSegments['FlightSegment']['ArrivalAirportLocationCode'],$getCityAirportCode)){
        $arrivalCity = $getCityAirportCode[$FlightSegments['FlightSegment']['ArrivalAirportLocationCode']].' - ';
      }
      
  	@endphp					
        <td>
            <div class="tour_icon cityboxIcon"><i class="ic-directions_bus"></i> </div>
             <div class="tour_info cityboxDetails mb-2">
             <strong>{{$leg['transport']['transport_name_text']}}</strong>
                <p class="mb-0"><strong>Departs</strong>: {{$FlightSegments['FlightSegment']['DepartureAirportLocationCode']}} {{$departCity}}{{$departure_time}} {{$departure_date}}</p>
                <p class="mb-0"><strong>Arrives</strong>:{{$FlightSegments['FlightSegment']['ArrivalAirportLocationCode']}} {{$arrivalCity}}{{$arrival_time}} {{$arrival_date}}</p>
                <p class="mb-0"><strong>Duration</strong>:{{ ucwords(str_replace('+', '', $leg['transport']['duration'])) }}</p>
            </div>    
        </td>
        <td class="text-center">${{$currency}} <span class="baseFLightPrice">{{$transport_price}}</span></td>
        <td class="text-center">
            <div class="price">
                {{$adults}} Adult{{!empty($children) ? ', '.$children.' Child':''}}
                
        </td>
      @endif
      @endif

        <td>
            <div class="text-right">
                <input id="baseFLightPriceID" type="hidden" value="{{$transport_price}}" >
                <strong>${{$currency}} <span class="baseFLightPrice">{{$transport_price}}</span></strong><br>
                <a href="javascript:void(0)" class="baggage_fare_rules" data-fare="{{$leg['transport']['fare_source_code']}}">Fare Rules</a><br>
                <a href="javascript:void(0)" class="disable_item_custom">Remove From Itinerary</a>
            </div>
        </td>
    </tr>



  @if(isset($ExtraServices) && !empty($ExtraServices))
       
       @foreach($ExtraServices as $keys => $value)
          @if(isset($value['Service']))
            <tr>
              <td class="border-top-0" colspan="4">
                <div class="tour_icon cityboxIcon">&nbsp; </div>
                    <div class="tour_info cityboxDetails mb-2">
                       <div class="row">
                          <div class="col-sm-12">
                            <div class="">
                              <h5 class="title_sub pl-2"> Extra Services</h5>
                               <div class="signup_forms mb-0  py-3 px-2 scroll_open black-checkbox">

                                @foreach($value['Service'] as $key1 => $value1)
                               
                                    <div class="form-group mb-0 mt-0">
                                        <label class="custom_check mt-0">&nbsp;
                                          @if(in_array($value1['ServiceId'],$serviceId))
                                             <input type="checkbox" class="extraServiceId" value="{{$value1['ServiceId']}}" checked>
                                          @else
                                             <input type="checkbox" class="extraServiceId" value="{{$value1['ServiceId']}}">
                                          @endif 
                                         
                                       
                                          <span class="check_indicator">&nbsp;</span>
                                        </label>
                                        <input type="hidden" value="{{$value1['ServiceCost']['Amount']}}" class="extraServiceClass">
                                        <span class="mr-3">
                                        {{str_replace($value1['ServiceCost']['Amount'],$CurrecySybmbol.$value1['ServiceCost']['Amount'],str_replace("||","",$value1['Description']))}}
                                      </span>
                                         <span class="mr-3">{{$value1['Behavior']}}</span>
                                    </div>
                                  
                              @endforeach
                              </div>
                            </div>    
                          </div>  
                       </div> 
                    </div> 
                     <div class="p-2 my-4"> 
                        <button class="btn  btns_input_dark btn_update_extra_service" data-leg="{{$key}}">SUBMIT</button> 
                    </div> 
             </td>  
            </tr>       
          @endif
        @endforeach
    @endif

@else
  <tr>
      <td colspan="4">
        <div class="tour_icon cityboxIcon"><i class="ic-directions_bus"></i> </div>
        <div class="tour_info cityboxDetails">
          <strong>Flight No longer Available</strong></br></br>
        </div>
      </td>
    </tr>
@endif 
  <div class="modal fade bd-example-modal-lg" id="info-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Baggage and Fare Rules</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body" id="fareRuleModal">
                                                                  
                </div>
            </div>
        </div>
    </div>