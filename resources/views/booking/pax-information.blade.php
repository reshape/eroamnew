@extends('layouts.home')

@section('custom-css')
<link rel="stylesheet" href="{{ url('css/booking.css') }}">
@stop

@section('content')
<div class="row">
	<div class="col-md-10 col-md-offset-1">
		@include('booking.steps')
		<div class="bordered-box col-md-10 col-md-offset-1">
			<h5 class="uppercase medium-txt">CLICK ON EACH PANEL TO ENTER PAX DETAILS</h5>
			<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				@for ($i = 0; $i <session()->get('search_input')['travellers']; $i++ )
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $i }}" aria-expanded="true" aria-controls="collapse-{{ $i }}">
									@if ($i == 0)
										<i class="flaticon-crown"></i> {{ isset($pax[$i]) ? $pax[$i]['firstname'].' '.$pax[$i]['lastname'] : 'LEAD PAX' }}
									@else
										<i class="flaticon-profile"></i> {{ isset($pax[$i]) ? $pax[$i]['firstname'].' '.$pax[$i]['lastname'] : 'NON-LEAD PAX' }}
									@endif

									@if (isset($pax[$i]))
										<span class="pull-right"><i class="flaticon-approve-circular-button"></i></span>
									@endif
								</a>
							</h4>
						</div>
						<div id="collapse-{{ $i }}" class="panel-collapse collapse {{ $i == 0 ? 'in' : '' }}" role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body">
								<form class="pax-information-form">
									<div class="row">
										<div class="col-md-6">
											<div class="form-group">
												<label for="first-name-{{ $i }}">First Name</label>
												<input type="text" name="firstname" id="first-name-{{ $i }}" class="form-control" value="{{ isset($pax[$i]) ? $pax[$i]['firstname'] : '' }}" tabindex="1" placeholder="Enter first name" pattern="[A-Za-z]{1,}" title="This field accepts only characters(a-z)." required>
											</div>
											<div class="form-group">
												<label for="title-{{ $i }}">Title</label>
												<select name="pax_title" id="title-{{ $i }}" class="form-control" tabindex="3" required>
														<option disabled="disabled" value="" selected="selected"> - Select Title - </option>
														<option value="Mr" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Mr' ? 'selected' : '' }}>Mr</option>
														<option value="Ms" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Ms' ? 'selected' : '' }}>Ms</option>
														<option value="Mrs" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Mrs' ? 'selected' : '' }}>Mrs</option>
														<option value="Miss" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Miss' ? 'selected' : '' }}>Miss</option>
														<option value="Mstr" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Mstr' ? 'selected' : '' }}>Mstr</option>
														<option value="Dr" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Dr' ? 'selected' : '' }}>Dr</option>
														<option value="Prof" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Prof' ? 'selected' : '' }}>Prof</option>
														<option value="Rev" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Rev' ? 'selected' : '' }}>Rev</option>
														<option value="Lord" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Lord' ? 'selected' : '' }}>Lord</option>
														<option value="Lady" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Lady' ? 'selected' : '' }}>Lady</option>
														<option value="Sir" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Sir' ? 'selected' : '' }}>Sir</option>
														<option value="Capt" {{ isset($pax[$i]) && $pax[$i]['pax_title'] == 'Capt' ? 'selected' : '' }}>Capt</option>
												</select>
											</div>
											<div class="form-group">
												<label for="dob-{{ $i }}">Date of Birth</label>
												<input type="text" name="dob" id="dob-{{ $i }}" class="form-control" value="{{ isset($pax[$i]) ? $pax[$i]['dob'] : '' }}" tabindex="5" placeholder="Select DOB" required>
											</div>
											<div class="form-group">
												<label for="email-{{ $i }}">Email Address</label>
												<input type="email" name="email" id="email-{{ $i }}" class="form-control" value="{{ isset($pax[$i]) ? $pax[$i]['email'] : '' }}" tabindex="7" placeholder="Enter email address" required>
											</div>
										</div>
										<div class="col-md-6">
											<div class="form-group">
												<label for="last-name-{{ $i }}">Last Name</label>
												<input type="text" name="lastname" id="last-name-{{ $i }}" class="form-control" value="{{ isset($pax[$i]) ? $pax[$i]['lastname'] : '' }}" tabindex="2" placeholder="Enter last name" pattern="[A-Za-z]{1,}" title="This field accepts only characters(a-z)." required>
											</div>
											<div class="form-group">
												<label for="pax-gender-{{ $i }}">Gender</label>
												<select id="pax-gender-{{ $i }}" name="pax_gender" class="form-control" tabindex="4" required>
													<option disabled="disabled" value="" selected="selected"> - Select Gender - </option>
													<option value="male" {{ isset($pax[$i]) && $pax[$i]['pax_gender'] == 'male' ? 'selected' : '' }}>Male</option>
													<option value="female" {{ isset($pax[$i]) && $pax[$i]['pax_gender'] == 'female' ? 'selected' : '' }}>Female</option>
												</select>
											</div>
											<div class="form-group">
												<label for="nationality-{{ $i }}">Nationality</label>
												<select name="nationality" id="nationality-{{ $i }}" class="form-control" tabindex="6" required>
													<option disabled="disabled" value="" selected="selected"> - Select Nationality - </option>
													@foreach( $featured_nationalities as $featured )
														<option value="{{ $featured->name }}" {{ isset($pax[$i]) && $pax[$i]['nationality'] == $featured->name ? 'selected' : '' }}>{{ $featured->name }}</option>
													@endforeach
													<option disabled="disabled">---------------</option>
													@foreach( $nationalities as $nationality )
														<option value="{{ $nationality->name }}" {{ isset($pax[$i]) && $pax[$i]['nationality'] == $nationality->name ? 'selected' : '' }}>{{ $nationality->name }}</option>
													@endforeach
												</select>
											</div>
											<div class="form-group">
												<label for="contact-no-{{ $i }}">Contact #</label>
												<input type="number" name="contactno" id="contact-no-{{ $i }}" value="{{ isset($pax[$i]) ? $pax[$i]['contactno'] : '' }}" class="form-control" tabindex="8" placeholder="Enter contact number" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-12 text-right">
											<button class="save-btn">Save Details</button>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				@endfor
			</div>
			<a href="#" style="{{ $filled ? 'display: inline-block' : 'display: none' }}" id="pax-continue-btn" class="continue-btn pull-right">Continue <i class="fa fa-angle-double-right"></i></a>
		</div>
	</div>
</div>
@stop

@section('custom-js')
<script type="text/javascript">
var searchSession;
$(document).ready(function() {
	searchSession = JSON.parse($('#search-session').val());

	for (var i = 0; i < parseInt(searchSession.travellers); i++) {
		var dob = $('#dob-'+i).val();
		if (dob != '') {
			var age = moment().diff(dob, 'years');
			if (age <= 17 && i != 0) {
				$('#email-'+i).prop('required', false);
				$('#contact-no-'+i).prop('required', false);
			} else {
				$('#email-'+i).prop('required', true);
				$('#contact-no-'+i).prop('required', true);
			}
		}
	}

	// Datepicker for DOB
	$('input[name="dob"]').datetimepicker({
		maxDate: moment().format('YYY-MM-DD'),
		format: 'Y-m-d',
		timepicker: false,
		onChangeDateTime: function(date, element) {
			var age = moment().diff(date, 'years');
			var email = $(element).parent().next().find('input[name="email"]');
			var contactNumber = $(element).parent().parent().next().find('input[name="contactno"]');
			var index = $('input[name="dob"]').index($(element));

			// if age is 17 below, don't require email/contact #
			if (age <= 17 && index != 0) {
				email.prop('required', false);
				contactNumber.prop('required', false);
			} else {
				email.prop('required', true);
				contactNumber.prop('required', true);
			}
		}
	});

	$('#accordion').on('shown.bs.collapse', function(e) {
		var target = $(e.target);
		target.find('.panel-body input[name="firstname"]').focus(); // focus on first name textbox when opening a panel
	});

	$('.pax-information-form').on('submit', function(e) {
		e.preventDefault();
		var formData = $(this).serializeArray();
		var index = $('.pax-information-form').index(this);
		var paxInformation = {};
		var button = $(this).find('.save-btn');

		for (var i = 0; i < formData.length; i++) {
			var data = formData[i];
			paxInformation[data.name] = data.value;
			paxInformation['lead_person'] = index == 0 ? 'yes' : 'no';
		}

		// save to session
		searchSession.pax_information[index] = paxInformation;
		eroam.ajax('post', 'save-booking', {search: JSON.stringify(searchSession)}, function(res) {
			button.html('Save Details').prop('disabled', false);

			// UPDATE PANEL
			var icon = index == 0 ? 'flaticon-crown' : 'flaticon-profile';
			var name = paxInformation.firstname + ' ' + paxInformation.lastname;
			$('.panel-title').eq(index).find('a').html('<i class="'+icon+'"></i> '+ name +'<span class="pull-right"><i class="flaticon-approve-circular-button"></i></span>');

			// show next panel
			if (index + 1 < $('.panel-title').length) {
				var next = index+1;
				$('#collapse-'+ next).collapse('show');
			}
			$('#collapse-'+index).collapse('hide');

			// check if all pax details are filled
			if (searchSession.pax_information.length == parseInt(searchSession.travellers)) {
				$('#pax-continue-btn').show();
			}
		}, function() {
			button.html('<i class="fa fa-spin fa-circle-o-notch"></i> Saving...').prop('disabled', true);
		});
	});
});
</script>
@stop