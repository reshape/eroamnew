<div class="itinerary_right">
    @include('itinenary.partials.sidebarstrip')
    @php 
        $BedTypeId = '@id';
    	$size = '@size';
    	$selectedPrice = 0;
	  	$hotelID = 0;
	  	$rateCode = 0;
	  	$search_session = json_decode(json_encode( session()->get( 'search' ) ) , FALSE );
	  	$travellers = $search_session->travellers;
	  	$searchRooms = $search_session->rooms;
	  	if($data->provider == 'expedia' && (isset($search_session->itinerary[$data->leg]->hotel->provider) && $search_session->itinerary[$data->leg]->hotel->provider == 'expedia')){
	    	$total = '@total';
	    	if($search_session->itinerary[$data->leg]->hotel ){
	      		$selectedPrice = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->RateInfos->RateInfo->ChargeableRateInfo->$total);
	      		$hotelID = $search_session->itinerary[$data->leg]->hotel->hotelId;
	      		$rateCode = ceil($search_session->itinerary[$data->leg]->hotel->RoomRateDetailsList->RoomRateDetails->rateCode);
	    	}
	  	}
	  
	  	$surcharge_components = array(
	    	'TaxAndServiceFee'=>'Tax Recovery & Service Fee',
	    	'ExtraPersonFee'=>'Extra Person Fee',
	    	'Tax'=>'Tax',
	    	'ServiceFee'=>'Service Fee',
	    	'SalesTax'=>'Sales Tax',
	    	'HotelOccupancyTax'=>'Hotel Occupancy Tax',
	    	'PropertyFee'=>'Property Fee'
	    );

	  	$i = 0;

	  	if(isset($data->HotelRoomResponse)){
		  	if(!isset($data->HotelRoomResponse->rateCode)){
		    	$temp_array = $data->HotelRoomResponse;
		     	foreach ($data->HotelRoomResponse as $key => $value) {
			      	if($value->rateCode == $rateCode){
			        	if($key != 0){
			          		$temp_array[$i] = $data->HotelRoomResponse[$key];
			          		$temp_array[$key] = $data->HotelRoomResponse[$i];
			        	}
			      	}
		    	}
		    	$data->HotelRoomResponse = $temp_array;
		  	}else{
		    	$temp_array[0] = $data->HotelRoomResponse;
		    	$data->HotelRoomResponse = $temp_array;
		  	}
		}
        
        $travellers = session()->get( 'search' )['travellers'];   
        $total_childs = session()->get( 'search' )['child_total']; 
        $rooms = session()->get( 'search' )['rooms']; 
        $room_text = "Room";

        $eroamPercentage = Config::get('constants.ExpediaEroamCommissionPercentage');
		$nights = 0;

		if (session()->get('search')){
		    $nights = session()->get('search')['itinerary'][$data->leg]['city']['default_nights'];
        }

        if($rooms > 1){
        	$room_text = "Rooms";
        }

        $bedTypeId = $rateCode = ''; 
        if (isset(session()->get( 'search' )['itinerary'][$data->leg]['hotel']['bedTypeId'])){
        	$bedTypeId = session()->get( 'search' )['itinerary'][$data->leg]['hotel']['bedTypeId'];
    	}

    	if(isset(session()->get( 'search' )['itinerary'][$data->leg]['hotel']['RoomRateDetailsList']['RoomRateDetails'])){
    		$RoomRateDetails = json_decode(json_encode(session()->get( 'search' )['itinerary'][$data->leg]['hotel']['RoomRateDetailsList']['RoomRateDetails']), true);
	    	if (isset($RoomRateDetails['rateCode'])){
				$rateCode = $RoomRateDetails['rateCode'];
	    	}
    	}
    	
    	$childrenAge = '';

        //echo '<pre>'; print_r(session()->get( 'search_input' )); echo '</pre>';
    	if(isset(session()->get( 'search_input' )['child'])){
    		$i=0;
	    	$children = session()->get( 'search_input' )['child'];
	    	foreach($children as $key => $child){
	    		foreach($child as $childKey => $childValue){
	    			$i++;
	    			$childrenAge .=  '1 x Child (Age '.$childValue.')<br>';
                    //$childrenAge .=  session()->get( 'search_input' )['child'][$key][$childKey];
	    		}
	    	}	
	    	$childrenAge = substr($childrenAge,0,-4);
	    }
    @endphp
    <?php //echo '<pre>'; print_r($data); die; ?>
    <?php //echo '<pre>'; print_r(session()->get( 'search' )); die; ?>
<style type="text/css">
	.accommodation_accordion ul.deluxe_room li strong {
	    min-width: 150px;
	}
</style>
    <div class="container-fluid">
    	<div class="pl-3">
            <div class="accommodation_top pt-4 pb-3 border-bottom">
    			<div class="row">
        			<div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <a href="{{ url( $data->searchCity.'/hotels?leg='.$data->leg ) }}"><i class="ic-navigate_before"></i> <strong>@lang('home.accommodation_back_link')</strong></a>
        			</div>
        			<div class="col-12 col-sm-12 col-md-8 col-lg-8 text-sm-left text-lg-right">
            			<i class="ic-place"></i>{{$data->searchCity}}, {{$data->country_name}}: {{date('l d M Y',strtotime($data->arrivalDate))}} - {{date('l d M Y',strtotime($data->departureDate))}} ({{$rooms}} {{$room_text}})
        			</div>
    			</div>
            </div>
            <div class="hotel_listtitle mt-4">
                <div class="hotlename pb-2   mb-3 ">
                    <div class="row">
                       
                        <div class="col-12 col-sm-4 col-md-6 col-lg-6 col-xl-6">
                            <div class="cat_name"> {{$data->hotelName}} </div>
                            <div class="pt-3"><i class="ic-place"></i> @lang('home.location'): {{$data->country_name}}, {{$data->searchCity}}  {{($data->hotelCity) ? '/'.$data->hotelCity : ''}}</div>
                        </div>
                        <div class="col-12 col-sm-8 col-md-6 col-lg-6 col-xl-6">
                            <div class="float-right">
                                <div class="night_charge">
                                    <p class="text-right pr-2 pt-5 mb-0 "> @lang('home.activity_label8')<strong> $AUD {{$data->hotelPrice}} </strong> P.P (Per Night)</p>
                                </div>
                                <div class="dayes text-center border-left ">
									@php
										$nights = sprintf("%02d", $nights);
									@endphp
                                    <span> {{$nights}}</span> {{($nights > 1 ? 'Nights':'Night')}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @if($data->HotelImages->HotelImage)
                    <div class="owl-carousel owl-theme">                 
                		@if(is_array($data->HotelImages->HotelImage))
	                     	@foreach ($data->HotelImages->HotelImage as $key => $value)
	                        	@if ($key <= 30)
			                        <div class="item"><img src="{{$value->url}}" alt=" " /> </div>
	                        	@endif
	                      	@endforeach
                		@else
                			<div class="item"><img src="{{$data->HotelImages->HotelImage->url}}" alt=" " /> </div>
                		@endif
                    </div>
                    @else
                        <div class="item"><img style="width: 258px; height: 197px;" src="{{Config::get('constants.AWSUrl')}}hotels/no-image.jpg" alt=" " /> </div>
                    @endif
                </div>
            </div>
            <div class="row pt-5">
                <div class="col-12 accommodation_tabs">
                    <ul class="nav nav-tabs border-0 justify-content-md-center" id="myTab" role="tablist">
                        <li class="nav-item pr-2  text-center">
                            <a class="nav-link border-0 active text-uppercase" id="home-tab" data-toggle="tab" href="#room_rates" role="tab" aria-controls="room_rates" aria-selected="true"> @lang('home.accommodation_heading1')  </a>
                        </li>
                        <li class="nav-item pr-2 pl-2 text-center">
                            <a class="nav-link border-0 text-uppercase" id="profile-tab" data-toggle="tab" href="#overview" role="tab" aria-controls="overview" aria-selected="false">@lang('home.overview_text_heading')</a>
                        </li>
                        <li class="nav-item  pl-2 text-center">
                            <a class="nav-link border-0 text-uppercase" id="contact-tab" data-toggle="tab" href="#infomportant" role="tab" aria-controls="infomportant" aria-selected="false">@lang('home.important_info')</a>
                        </li>
                    </ul>
                </div>
                <div class="col-12">
                    <div class="tab-content accommodation_tabs_content pb-5" id="myTabContent">
                        <div class="tab-pane fade show active" id="room_rates" role="tabpanel" aria-labelledby="home-tab">
                            <div class="pt-3 pt-5 pb-5 ">
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="accordion accommodation_accordion" id="accordionExample">
                                        	@include('accomodation.partials.accomodation_detail_room_rate')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="overview" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="locations_map pt-3 pt-5 pb-5 ">
                                    	<div id="map1" style="height: 400px; width:100%; position: relative !important;" class="map_detail"></div>
                    					<input id="pac-input" class="form-control full-width" type="text" readonly data-lat="{{ $data->latitude != '' ? $data->latitude : '' }}" data-lng="{{ $data->longitude != '' ? $data->longitude : '' }}" value="{{ $data->google_map_location != '' ? $data->google_map_location : '' }}" placeholder="Search Box" style="display:none;"> 
                                    	<div id="infowindow-content">
					                    	<img src="" width="16" height="16" id="place-icon">
					                    	<span id="place-name" class="place-title"></span><br>
					                    	<span id="place-address"></span>
					                    </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-12">
                                	@if(isset($data->HotelDetails->locationDescription))
	                                    <div class="pt-3">
	                                        <strong> Location</strong>
						                    <p>{{$data->HotelDetails->locationDescription}}</p>
	                                    </div>
				                    @elseif($data->location_description)
				                      	<div class="pt-3">
                                            <strong> Location Description</strong>
	                                        <p>{!! $data->location_description !!}</p>
	                                    </div>
				                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="infomportant" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="pt-3 pt-5 pb-5">
                            	@include('accomodation.partials.accomodation_detail_imp_info')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    $('.childTooltip').tooltip({html: true}); 
});
</script>