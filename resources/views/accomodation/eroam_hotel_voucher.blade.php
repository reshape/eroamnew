<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $aData['leg'][$leg]['from_city_name'].'_HotelVoucher_'.$aData['invoiceNumber'].'.pdf';?></title>
    <style type="text/css">
      @page {
        margin: 15px;
      }
      body{
        font-family: Arial;
        color: #212121;
        font-size: 14px;
        margin: 0px;
      }
      p{
        margin: 5px 0;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <?php 
      $logo = getDomainLogo();
      if(!$logo) {
              $logo = public_path('/images/logo-big.png');
      }
    ?>
    <table width="100%" cellpadding="4">
      <tbody>
        <tr>
          <td colspan="2">
            <table width="100%" style="background-color: #212121; color: #fff;">
              <tr>
                <td style="padding: 0 10px;">
                  <img src="{{ $logo }}" alt="eRoam" style="width: 100px;">
                </td>
                <td style="text-align: right; padding: 0px 15px; vertical-align: middle;">
                  <h2 style="margin-top: 4px;"><img src="{{ public_path('/images/ic_local_hotel.svg') }}" alt="" style="position: relative; top: 3px; margin-top: 4px;"/> Hotel Voucher</h2>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa;text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th colspan="3">
                    <h2 style="margin: 8px 8px 8px 2px;">Booking Details</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p><strong>Guest Name:</strong> {{$aRequestData['billing_first_name']}}</p>
                    <p><strong>Email:</strong> {{$aRequestData['billing_email']}}</p>
                  </td>
                  <td>
                    <p><strong>eRoam Booking Id:</strong> {{$aData['invoiceNumber']}}</p>
                    <p><strong>Booking Date:</strong> {{ date("F j, Y, g:i a") }}</p>
                  </td>
                </tr>
                <tr>
                  <td>
                      <h3 style="margin: 0;">{{$aData['leg'][$leg]['from_city_name'].', '.$aData['leg'][$leg]['country_code'].' - '.$aHotel['leg_name']}}</h3>
                  </td>
                  <td></td>
                </tr>
                <tr>
                  <td colspan="3">
                    <strong>Hotel Address</strong><br>
                    {{$aHotel['address']}}<br>{{$aData['leg'][$leg]['from_city_name'].', '.$aData['leg'][$leg]['country_code']}}
                  </td>
                </tr>
                <tr>
                  <td colspan="3">
                    <h3 style="margin: 5px 0">Total payment to be made - {{ $aHotel['currency'] }} {{ $aHotel['hotel_expedia_total'] }}</h3>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%" border="1" style="border: solid 3px #fafafa; text-align: center; border-collapse: collapse; margin-top: 5px;" cellspacing="4" cellpadding="10">
              <tr>
                <td style="border-color:#fafafa;">
                  <h3 style="margin: 0;">Check-in Date<!--  &amp; Time --></h3>
                  <p>{{ date("F j, Y",strtotime($aHotel['checkin'])) }} {{--$aHotel['checkin']--}}</p>
                  <!-- <p>12 PM Onwards</p> -->
                </td>
                <td style="border-color:#fafafa;">
                  <h3 style="margin: 0;">Nights</h3>
                  <p>{{ $aHotel['nights'] }} Nights</p>
                </td>
                <td style="border-color:#fafafa;">
                  <h3 style="margin: 0;">Check-out Date<!--  &amp; Time --></h3>
                  <p>{{ date("F j, Y",strtotime($aHotel['checkout'])) }}{{--$aHotel['checkout']--}}</p> 
                  <!-- <p>Till 11 AM</p> -->
                </td>
                <td style="border-color:#fafafa;">
                  <h3 style="margin: 0;">Guests</h3>
                  <p>{{ $aData['adult'] }} Adult {{!empty($aData['child']) ? ', '.$aData['child'].' Child':''}}</p>
                </td>
                <td style="border-color:#fafafa;">
                  <h3 style="margin: 0;">Rooms</h3>
                  <p>{{$aHotel['hotel_total_room']}} {{($aHotel['hotel_total_room'] > 1) ? 'Rooms':'Room'}}</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>

        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="3">
                      <h2 style="margin: 8px 8px 8px 2px;">Room Details</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                    $passenger = session()->get('search_input')['pax'];

                    for($i = 0; $i < $aHotel['hotel_total_room']; $i++){
                      $roomtype = '<tr><td><h3 style="margin: 5px 0">Room '.($i+1).' </h3>';
                      $roomtype .= '<table>';
                      $roomtype .= '<tr><td width="50px">&nbsp;</td><th>Room Type</th><th>:</th><td>'.$aHotel['hotel_room_type_name'].'</td></tr>';
                      
                      $roomtype .= '<tr><td></td><th>Passengers Detail</th><th>:</th><td>';
                      if(isset($passenger[$i])){
                        $j=0;
                        foreach ($passenger[$i] as $pass) {
                          $j++;
                          $roomtype .= ucwords($pass['firstname'].' '.$pass['lastname']);//.' ('.$pass['age'].')';
                          if($pass['child'] == 1){
                            if($pass['age'] == 0){ $pass['age'] = 1; }
                            $roomtype .= ' ('.$pass['age'].($pass['age'] > 1 ? ' Years':' Year').')';
                          }
                          if($j < count($passenger[$i])) { $roomtype .= ', ';} else { $roomtype .= '</p>';}
                        }
                      }

                      $roomtype .='</td></tr></table>';
                      $roomtype .= '</td></tr>';
                      echo $roomtype;
                    }
                  ?>  
                </tbody>
              </table>
            </td>
        </tr>

        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th colspan="3">
                      <h2 style="margin: 8px 8px 8px 2px;">Payment Summary</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <strong>Room Tariff</strong>
                    </td>
                    <td>
                      
                        {{ $aHotel['currency'] }} {{ $aHotel['hotel_price_per_night'] }} x {{$aHotel['nights']}} Nights x {{$aHotel['hotel_total_room']}} Rooms 
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                        {{$aHotel['currency'].' '.$aHotel['hotel_expedia_total']}}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Other Charges</strong><br>
                      Tax Recovery & Service Fee<br>
                      Hotel Fees
                    </td>
                    <td><br>
                      {{ $aHotel['currency'].' '.number_format($aHotel['taxes'],2) }}<br>
                      {{ $aHotel['currency'].' '.number_format($aHotel['hotel_tax'],2) }}
                    </td>
                    <td style="text-align: right; padding-right: 20px;">	
                       <strong>{{$aHotel['currency']}}
                        <?php 
                          $aHotel['hotel_eroam_subtotal'] = (float)$aHotel['hotel_eroam_subtotal'];
                          $totaltax = $aHotel['taxes'] + $aHotel['hotel_tax'];
                          $totaltax = number_format($totaltax,2);
                          echo $totaltax;
                        ?></strong><br>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Grand Total</strong>
                    </td>
                    <td></td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{$aHotel['currency']}} {{ number_format($aHotel['hotel_eroam_subtotal'],2) }}</strong>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Advance Paid</strong>
                    </td>
                    <td>
                      Paid through Credit Card ({{ $aHotel['currency'].' '.number_format($aHotel['hotel_eroam_subtotal'],2) }})
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      {{$aHotel['currency']}} {{ number_format($aHotel['hotel_eroam_subtotal'],2) }}
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <strong>Total Payable Amount </strong><br>All Inclusive
                    </td>
                    <td>
                      <strong>Pay at Check-in</strong>
                    </td>
                    <td style="text-align: right; padding-right: 20px;">
                      <strong>{{ $aHotel['currency'].' '.number_format($aHotel['hotel_tax'],2) }}{{--$aHotel['currency'].' '.number_format($aHotel['hotel_eroam_subtotal'],2)--}}</strong>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
        </tr>

        @isset($aHotel['cancellationPolicy'])
        <tr>
            <td colspan="2">
              <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
                <thead>
                  <tr style="background-color: #fafafa;">
                    <th>
                      <h2 style="margin: 8px 8px 8px 2px;">Cancellation &amp; Amendment Policy</h2>
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <ul>
                        <li>{!!html_entity_decode($aHotel['cancellationPolicy'])!!}</li>
                      </ul>
                    </td>
                  </tr>
                  
                </tbody>
              </table>
            </td>
        </tr>
        @endisset

        @isset($aHotel['checkin_instruction'])
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th>
                    <h2 style="margin: 8px 8px 8px 2px;">@lang('home.hotel_checkin_instruction_text')</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>
                      @php
                        $aHotel['checkin_instruction'] = strstr($aHotel['checkin_instruction'], '<p><b>Fees</b>');
                        $aHotel['checkin_instruction'] = str_replace('<p><b>Fees</b>','<p><strong>Fees / Optional Extras</strong>',$aHotel['checkin_instruction']);
                      @endphp
                      {!!html_entity_decode($aHotel['checkin_instruction'])!!}
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @endisset

        @isset($aHotel['amenities_description'])
        <tr>
          <td colspan="2">
            <table width="100%" style="border: solid 3px #fafafa; text-align: left; margin-top: 5px;" cellspacing="4" cellpadding="6">
              <thead>
                <tr style="background-color: #fafafa;">
                  <th>
                    <h2 style="margin: 8px 8px 8px 2px;">Amenities Instructions</h2>
                  </th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <p>{!!html_entity_decode($aHotel['amenities_description'])!!}</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        @endisset
        <tr>
          <td>
            <p style="margin-top: 40px; font-size: 16px;">We wish you a hassle-free stay.<br>
              Waiting to host you</p>
              
            <p style="margin-top: 20px; font-size: 16px;">See you soon,<br>
              <strong>Team eRoam</strong></p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>