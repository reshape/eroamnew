<!-- START ALERT -->
<div class="modal fade no-flight-summary" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"></h4>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                      <p>Sorry, some of selected flight no longer available for now.
                        <br>
                        Please, select other flights and proceed.
                    </p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn confirm-cancel-button eroam-confirm-cancel-btn" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

